'use strict';

// let num = parseInt(prompt('Число', ''));
//  let promise = new Promise((resolve, reject) => {
//         if (isNaN(num)) {
//             reject(alert('Не число'));
//             return;
//         }
//    let interval = setInterval(() => {
//             num = num - 1;
//             console.log(`Осталось ${num}`);

//             if (num === 0) {
//             clearInterval(interval);
//             resolve(console.log('Время вышло')); 
//         } 
//     }, 1000);
//  });

//  promise
//  .then(() => {
//     console.log('Обещание выполнено')
//  })
//  .catch(() => {
//     console.log('Ошибка');
//  });


// 2 упражнение
let promise = fetch('https://reqres.in/api/users');
 console.log(promise);

promise
.then((response) => {
    return response.json();
})
.then((response) => {
console.log(response.data);

    let users = response.data;

    console.log(`Получили пользователей: ${users.length}`)
    users.forEach((user) => {
        console.log( `-${user.first_name} ${user.last_name} (${user.email})`)
    })


})
.catch(() => {
    console.log('Не удалось выполнить запрос')
});


  
