'use strict';

//1 Упражнение

/**
 * 
 * @param {string} obj - Проверяемый объект
 * @returns {boolean} - Возвращаем проверку на содержание ключей в объекте
 */
// function isEmpty(obj) {
//     for (let key in obj) {
//         return false;
//     };
//     return true;
// };

// let Max = {};
// alert(isEmpty(Max));

//2 упражение в data.js

//3 упражнение

/**
 * 
 * @param {number} perzent - Процент на который повышаем значения
 * @return {number} - Возвращаем сумму значений умноженных на perzent
 */
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
  };
  
  function raiseSalary(perzent) { 
    let raising = 0;
    let sum = 0;
  for (let key in salaries) {
    raising = salaries[key] * perzent / 100;
    sum += salaries[key] + raising;
  }
  return sum;
  }
  console.log(raiseSalary(10));