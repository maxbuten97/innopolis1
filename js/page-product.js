'use strict';
const formName = document.querySelector('.form__name');
const formGrade = document.querySelector('.form__grade');
const errorName = document.querySelector('.error_name-error');
const errorGrade = document.querySelector('.error_grade-error');
const inputForm = document.querySelector('.form__footer');
const formContent = document.querySelector('.form__content');
const form = document.querySelector('.form');

//homework-24
// Код с ошибкой при вводе полей
inputForm.addEventListener('click', (e) => {
    e.preventDefault();
    if (formName.value.length === 0) {
        errorName.style.visibility = 'visible';
        errorName.innerHTML = `Вы забыли указать имя и фамилию`;
    } 
     if (formName.value.length <= 2) {
        errorName.style.visibility = 'visible';
        errorName.innerHTML = `Имя не может быть короче 2-х символов`;
    }
    if(formGrade.value.length === 0) {
        errorGrade.style.visibility = 'visible';
        errorGrade.innerHTML = `Введите оценку`;
    } 
     if (formGrade.value <= 1 && formGrade.value >= 5) {
    errorGrade.style.visibility = 'visible';  
    errorGrade.innerHTML = `Оценка должна быть от 1 до 5`;
    }
});
// Ошибка при вводе имени
formName.addEventListener('input', (e) => {
     e.preventDefault();
     if(e.target.value.length === 0) {
        errorName.style.visibility = 'visible';
        errorName.innerHTML = `Вы забыли указать имя и фамилию`;
     } else if(e.target.value.length <= 2) {
        errorName.style.visibility = 'visible';
        errorName.innerHTML = `Имя не может быть короче 2-х символов`;
    } else {
        errorName.style.visibility = 'hidden';    
    };
});
// Ошибка при вводе оценки
formGrade.addEventListener('input', (e) => {
    e.preventDefault();
    if(e.target.value.length === 0) {
        errorGrade.style.visibility = 'visible';
        errorGrade.innerHTML = `Введите оценку`;
    } else if (e.target.value >= 1 && e.target.value <= 5) {
    errorGrade.style.visibility = 'hidden';  
    errorGrade.innerHTML = `Оценка должна быть от 1 до 5`;
    } else {
    errorGrade.style.visibility = 'visible';
   };
});

//localStorage homework-25
// Сохранить Текст отзыва
formContent.value = localStorage.getItem('formContent');
formContent.oninput = () => {
  localStorage.setItem('formContent', formContent.value)
};
// Сохранить Имя 
formName.value = localStorage.getItem('formName');
formName.oninput = () => {
  localStorage.setItem('formName', formName.value)
};
// Сохранить Оценку
formGrade.value = localStorage.getItem('formGrade');
formGrade.oninput = () => {
  localStorage.setItem('formGrade', formGrade.value)
};
//Как сделать если пользователь
// отправил форму после обновления страницы форма должна быть пустой
// Не сообразил